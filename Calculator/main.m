//
//  main.m
//  Calculator
//
//  Created by Nikita Semistrok on 31/07/16.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
