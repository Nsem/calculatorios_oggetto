//
//  AppDelegate.h
//  Calculator
//
//  Created by Nikita Semistrok on 31/07/16.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

