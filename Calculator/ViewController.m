//
//  ViewController.m
//  Calculator
//
//  Created by Nikita Semistrok on 31/07/16.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "ViewController.h"
enum {plus = 101, minus = 102, multi = 103, divi = 104, tempClear = 200, tempAdd = 201, tempRemove = 202, tempWrite = 203};


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clear:(id)sender{
    
    firstNum = 0;
    [self tablo];
    
}

- (IBAction)allClear:(id)sender{
    firstNum = 0;
    secondNum = 0;
    tempNum = 0;
    operationEnter = NO;
    doubleOperation = NO;
    [self tablo];
    
}

- (IBAction)num:(id)sender{
    if (operationEnter){
        secondNum = firstNum;
        firstNum = 0;
        operationEnter = NO;
    }
    
    firstNum = (firstNum * 10.0f) + [sender tag];
    [self tablo];
    NSLog(@"%li", (long)[sender tag]);
}

- (IBAction)operation:(id)sender{
    NSLog(@"%li", (long)[sender tag]);
    
    if (doubleOperation && !operationEnter){
        switch (operation) {
                
            case plus:
                firstNum = secondNum + firstNum;
                break;
                
            case minus:
                firstNum = secondNum - firstNum;
                break;
                
            case multi:
                firstNum = secondNum * firstNum;
                break;
                
            case divi:
                firstNum = secondNum / firstNum;
                break;
                
            default:
                break;
        }
    }   
    
    secondNum = firstNum;
    
    operation = [sender tag];
    operationEnter = YES;
    doubleOperation = YES;
    [self tablo];
}

- (IBAction)inverse:(id)sender{
    
    firstNum = -firstNum;
    [self tablo];
    
}

- (IBAction)memory:(id)sender{
    NSLog(@"%li", (long)[sender tag]);
    if (operation == tempAdd){
        tempNum = tempNum + firstNum;
    }
    
    if (operation == tempWrite){
        firstNum = tempNum;
        [self tablo];
    }
    
    if (operation == tempRemove){
        tempNum = tempNum - firstNum;
    }
    
    operation = [sender tag];
}

- (void) tablo{
    NSString *str = [NSString stringWithFormat: @"%g", firstNum];
    [displayLabel setText:str];
}

@end
