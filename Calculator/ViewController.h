//
//  ViewController.h
//  Calculator
//
//  Created by Nikita Semistrok on 31/07/16.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import <UIKit/UIKit.h>
double firstNum, secondNum, tempNum;
NSInteger operation;
BOOL operationEnter;
BOOL doubleOperation;

@interface ViewController : UIViewController{
    
    IBOutlet UILabel *displayLabel;
}

- (IBAction)clear:(id)sender;

- (IBAction)allClear:(id)sender;

- (IBAction)num:(id)sender;

- (IBAction)operation:(id)sender;

- (IBAction)inverse:(id)sender;

- (IBAction)memory:(id)sender;

@end

